package piinkscan.cdc;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

import piinkscan.google.android.gms.samples.vision.barcode.multitracker.R;

public class ListBarcode extends AppCompatActivity {
    private ListView mListView;
    private ArrayAdapter aAdapter;
    String[] barcodes;
    ImageView send;
    Button valider;
    static ArrayList<String> listWithoutDuplicates;
    ArrayList<String> newweights;
    LinearLayout dialog;
    TextView nbrbarcode,totalpoids;
    static float total_poids;
    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_barcode);
        checkPermissions();
        dialog=(LinearLayout)findViewById(R.id.dialog);
        nbrbarcode = (TextView)findViewById(R.id.nombrebarcode);
        totalpoids = (TextView)findViewById(R.id.totalpoids);
        dialog.setVisibility(View.INVISIBLE);
        total_poids=0;
        newweights = new ArrayList<String>();
        try {
            barcodes = new ArrayList<>(Arrays.asList(readFile().split("-"))).toArray(new String[0]);
            LinkedHashSet<String> hashSet = new LinkedHashSet<String>(Arrays.asList(barcodes));
            listWithoutDuplicates = new ArrayList<>(hashSet);
            for (String l:listWithoutDuplicates) {
                String bar = removeLastChar(l);
                String lastfiveDigits;
                if (bar.length() > 5)
                {
                     lastfiveDigits = bar.substring(bar.length() - 5);
                }
                else
                {
                     lastfiveDigits = bar;
                }
                int lastnumber = Integer.parseInt(lastfiveDigits);
                float lastweight = (float) (lastnumber/1000.0);
                total_poids+=lastweight;
                newweights.add(String.valueOf(lastweight)+" kg");
            }
            mListView = (ListView) findViewById(R.id.listv);
            aAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, newweights);
            mListView.setAdapter(aAdapter);




        } catch (IOException e) {
            e.printStackTrace();
        }

        send=(ImageView)findViewById(R.id.send_btn);
        valider=(Button) findViewById(R.id.valider);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.setVisibility(View.VISIBLE);
                nbrbarcode.setText(String.valueOf("   Nombre du barcode détectés: "+listWithoutDuplicates.size()));
                totalpoids.setText(String.valueOf("   Total du Poids: "+total_poids+" Kg"));

            }
        });

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.setVisibility(View.INVISIBLE);
            }
        });


    }
    private static String removeLastChar(String str)
    {
        return str.substring(0, str.length() - 1);
    }
    private String readFile() throws IOException {
        File fileEvents = Environment.getExternalStorageDirectory();
        File f = new File(fileEvents,"barcodes.txt");
        StringBuilder text = new StringBuilder();

        BufferedReader br = new BufferedReader(new FileReader(f));
        String line;
        while ((line = br.readLine()) != null) {
            text.append(line);
        }
        br.close();

        String result = text.toString();
        //Toast.makeText(getApplicationContext(),result.toString(),Toast.LENGTH_LONG).show();
        return result;
    }



    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<>();
         for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
             final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }

                break;
        }
    }

}
