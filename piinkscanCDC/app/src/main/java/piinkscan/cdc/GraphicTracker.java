
package piinkscan.cdc;

import android.content.Context;
import android.support.annotation.UiThread;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.text.TextBlock;
import java.io.IOException;
import piinkscan.cdc.cameraUi.GraphicOverlay;

class GraphicTracker<T> extends Tracker<T> {
    private GraphicOverlay mOverlay;
    private TrackedGraphic<T> mGraphic;
    private BarcodeUpdateListener mBarcodeUpdateListener;
    GraphicTracker(GraphicOverlay overlay, TrackedGraphic<T> graphic, Context context) {
        mOverlay = overlay;
        mGraphic = graphic;

        if (context instanceof BarcodeUpdateListener) {
            this.mBarcodeUpdateListener = (BarcodeUpdateListener) context;
        } else {
            throw new RuntimeException("");
        }

    }


    public interface BarcodeUpdateListener {
        @UiThread
        void onBarcodeDetected(Detector.Detections detectionResults, boolean flag) throws IOException;
    }
    //@Override
    public void onNewItem(int id, Barcode  item) {
        mGraphic.setId(id);
    }
    @Override
    public void onUpdate(Detector.Detections<T> detectionResults, T item) {
        mOverlay.add(mGraphic);
        mGraphic.updateItem(item);

        if(item instanceof TextBlock ) {
            try {
                mBarcodeUpdateListener.onBarcodeDetected(detectionResults, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                mBarcodeUpdateListener.onBarcodeDetected(detectionResults, false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onMissing(Detector.Detections<T> detectionResults) {
        mOverlay.remove(mGraphic);
    }
    @Override
    public void onDone() {
        mOverlay.remove(mGraphic);
    }
}
