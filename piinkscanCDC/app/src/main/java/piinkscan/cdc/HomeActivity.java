package piinkscan.cdc;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import piinkscan.google.android.gms.samples.vision.barcode.multitracker.R;


public class HomeActivity extends AppCompatActivity {

    ImageView logo;
    private final int SPLASH_DISPLAY_LENGTH = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        logo=(ImageView)findViewById(R.id.imageView);
        Animation anim = AnimationUtils.loadAnimation(this,R.anim.splashanim);
        logo.startAnimation(anim);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                 Intent mainIntent = new Intent(HomeActivity.this, BarcodeTrackerActivity.class);
                HomeActivity.this.startActivity(mainIntent);
                HomeActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }






}