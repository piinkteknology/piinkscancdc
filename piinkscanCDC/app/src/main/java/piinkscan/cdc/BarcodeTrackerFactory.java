package piinkscan.cdc;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.os.Environment;

import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import piinkscan.cdc.cameraUi.GraphicOverlay;
import piinkscan.google.android.gms.samples.vision.barcode.multitracker.R;

class BarcodeTrackerFactory implements MultiProcessor.Factory<Barcode> {
    private GraphicOverlay mGraphicOverlay;
    private Context mContext;
    BarcodeTrackerFactory(GraphicOverlay graphicOverlay, Context context) {
        mGraphicOverlay = graphicOverlay;
        mContext = context;
    }

    @Override
    public Tracker<Barcode> create(Barcode barcode) {
        BarcodeGraphic graphic = new BarcodeGraphic(mGraphicOverlay);

        return new GraphicTracker<>(mGraphicOverlay, graphic, mContext);

    }
}

class BarcodeGraphic extends TrackedGraphic<Barcode> {

    private static int mCurrentColorIndex = 0;

    private Paint rectPaint,centerPaint;
    private Paint mTextPaint;
    private volatile Barcode mBarcode;
    static List barcodevalues;
    BarcodeGraphic(GraphicOverlay overlay) {
        super(overlay);
        barcodevalues = new ArrayList<>();
        rectPaint = new Paint();
        rectPaint.setColor(0x60209020);
        rectPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        rectPaint.setStrokeWidth(8.0f);
        rectPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY));
        rectPaint.setAntiAlias(true);

        centerPaint = new Paint();
        centerPaint.setColor(Color.RED);
        centerPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        centerPaint.setStrokeWidth(4);

        mTextPaint = new Paint();
        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setTextSize(36.0f);
        rectPaint.setMaskFilter(new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL));

    }

    void updateItem(Barcode barcode) {
        mBarcode = barcode;
        postInvalidate();
    }

    @Override
    public void draw(Canvas canvas) {
        Barcode barcode = mBarcode;
        if (barcode == null) {
            return;
        }
        if(barcode.rawValue.length()==13) {
            RectF rect = new RectF(barcode.getBoundingBox());
            rect.left = translateX(rect.left);
            rect.top = translateY(rect.top);
            rect.right = translateX(rect.right);
            rect.bottom = translateY(rect.bottom);
            canvas.drawRect(rect, rectPaint);
            canvas.drawText(barcode.rawValue, rect.left, rect.bottom, mTextPaint);

            insertbarcodesinfile(barcode.rawValue);

            if (barcodevalues.contains(barcode.rawValue)) ;
            else {
                MediaPlayer mediaPlayer;
                mediaPlayer = MediaPlayer.create(BarcodeTrackerActivity.mContext, R.raw.beep);
                mediaPlayer.start();
                barcodevalues.add(barcode.rawValue);
            }
        }
    }

    public void insertbarcodesinfile(String value)
    {
        File file = new File(Environment.getExternalStorageDirectory(), "");
        if (!file.exists()) {
            file.mkdir();
        }
        try {
            File piinkfile = new File(file, "barcodes.txt");
            FileWriter writer = new FileWriter(piinkfile,true);
            writer.write(value + "-");
            writer.flush();
            writer.close();
         } catch (Exception e) {
        }

    }

}


